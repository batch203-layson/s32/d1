let http = require("http");

port = 4000;

// Mock Database
let directory = [
	{
		"name":"Brandon",
		"email":"brandon@mail.com"
	},
	{
		"name":"Jobert",
		"email":"jobert@mail.com"
	}
	
]

const	server	= http.createServer((request, response)=>{
	// When the '/users' route is accessed with a method of "GET" we will send the directory(mock database) list of users
	if(request.url=="/users" && request.method=="GET")
	{
		response.writeHead(200, {"Content-Type":"application/json"});
		response.write(JSON.stringify(directory));
		response.end();
	}
	// We want to received the content of the request and save it to the mock database
	// parse the received JSON request body to JavaScript object
	// add the parse object to the directory(mock database)
	if(request.url == "/users" && request.method=="POST")
	{
		// this will act as a placeholder for the resource/data to be created later on
		let requestBody = "";

		// data step - this reads "data" steam and process it as a request body
		request.on("data", (data)=>{
			// data received
			console.log(data);

			// Assigns the data retrieved from the data to stream to requestBody
			// at this point, "requestBody" has the
			requestBody += data;
			// to show that the chunk of information is stored in the requestBody
			console.log(requestBody);
		})
		

	request.on("end", ()=>{
		console.log(typeof requestBody);
		requestBody = JSON.parse(requestBody);
		console.log(typeof requestBody);
		directory.push(requestBody);
		console.log(directory);

		response.writeHead(200, {"Content-Type": "application/json"});
		response.write(JSON.stringify(requestBody));
		response.end();
		});
	}
});

server.listen(port);


console.log(`Server running at localhost:${port}`);
